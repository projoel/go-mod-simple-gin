module gitlab.com/hikash/go-mod-simple-gin

go 1.12

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/kelseyhightower/envconfig v1.4.0
	gitlab.com/projoel/modmath v0.0.0-20190730224102-f2900e5cf637
)
