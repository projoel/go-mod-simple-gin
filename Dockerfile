FROM alpine

WORKDIR /app

COPY dist/ .

ENV NAME=Gitlab

CMD ["./app"]