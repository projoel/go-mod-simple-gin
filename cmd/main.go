package main

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/projoel/modmath/mathy"
)

type Response struct {
	Name     string `json:"name"`
	Greeting string `json:"greeting"`
}

type DoubleResponse struct {
	Given  int `json:"given"`
	Result int `json:"result"`
}

type CollatzResponse struct {
	Given  uint64   `json:"given"`
	Result []uint64 `json:"result"`
}

type ErrorResponse struct {
	Got    string `json:"got"`
	Wanted string `json:"wanted"`
}

func main() {
	port := 8080

	LoadConfig()

	router := gin.New()
	router.GET("/", func(c *gin.Context) {
		resp := Response{Name(), "Hi"}
		c.JSON(http.StatusOK, resp)
	})

	router.GET("/double/:value", func(c *gin.Context) {
		rawValue := c.Param("value")
		value, err := strconv.Atoi(rawValue)

		if err != nil {
			c.JSON(http.StatusBadRequest, ErrorResponse{rawValue, "Something that looks like an integer"})
			return
		}
		resp := DoubleResponse{
			Given:  value,
			Result: mathy.Double(value),
		}
		c.JSON(http.StatusOK, resp)
	})

	router.GET("/collatz/:value", func(c *gin.Context) {
		rawValue := c.Param("value")
		value, err := strconv.ParseUint(rawValue, 10, 64)

		if err != nil {
			c.JSON(http.StatusBadRequest, ErrorResponse{rawValue, "Something that looks like an unsigned integer"})
			return
		}
		getNextVal := mathy.CollatzGen(value)
		sequence := []uint64{value}
		nextVal := value

		for nextVal != 1 && nextVal != 0 {
			nextVal = getNextVal()
			sequence = append(sequence, nextVal)
		}
		resp := CollatzResponse{
			Given:  value,
			Result: sequence,
		}
		c.JSON(http.StatusOK, resp)
	})

	router.Run(fmt.Sprintf(":%v", port))
}
